//
//  MediaViewController.m
//  shipin8-ios
//
//  Created by gxw on 14-2-10.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "MediaViewController.h"

@interface MediaViewController ()
@property (nonatomic,retain) MPMoviePlayerController *moviePlayer;
@end

@implementation MediaViewController

- (void)loadView
{
    [super loadView];
    self.title = @"视频播放";
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    self.view.backgroundColor = [UIColor clearColor];
    
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:YES animated:NO];
    
    self.isOn = false;
    
    NSURL *movieURL = [NSURL URLWithString:@"http://58.196.13.10:9200/balm/uploads/product_video/attachment/42/42ae21d7-1a10-4192-99ea-a19fa5c060d2.mp4"];
    
    _moviePlayer=[[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    _moviePlayer.view.frame = [UIScreen mainScreen].bounds;
    _moviePlayer.controlStyle = MPMovieControlStyleFullscreen;
    
    //設定影片比例的縮放、重複、控制列等參數
    _moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    _moviePlayer.repeatMode = MPMovieRepeatModeNone;
    _moviePlayer.controlStyle = MPMovieControlStyleFullscreen;

//    _moviePlayer.scalingMode = MPMovieScalingModeNone;
//    [_moviePlayer setFullscreen:NO animated:NO];
    
    [self.view addSubview:_moviePlayer.view];
    [_moviePlayer play];
    
    //监听done事件
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myMovieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    
    //监听缩放事件
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myMovieExpand:)
                                                 name:MPMoviePlayerScalingModeDidChangeNotification
                                               object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// When the movie is done, release the controller.
-(void)myMovieFinishedCallback:(NSNotification*)aNotification
{
    NSLog(@"111111111111111");
  
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerScalingModeDidChangeNotification
                                                  object:nil];
    [_moviePlayer stop];
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)myMovieExpand:(NSNotification*)aNotification
{
    NSLog(@"222222222222222");
    
    if (self.isOn) {
        CGAffineTransform landscapeTransform = CGAffineTransformMakeRotation(0);
        _moviePlayer.view.transform = landscapeTransform;
        
        _moviePlayer.view.frame = self.view.frame;
        //自動縮放符合畫面比例
        _moviePlayer.view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.isOn = NO;
    } else {
        _moviePlayer.view.frame=CGRectMake(-126, 126, 568,320 );
        _moviePlayer.scalingMode = MPMovieScalingModeNone;
        
        CGAffineTransform landscapeTransform = CGAffineTransformMakeRotation(M_PI / 2);
        _moviePlayer.view.transform = landscapeTransform;
        self.isOn = YES;
    }
}

/**
 为了保证内部不泄露，在dealloc中释放占用的内存
 */
- (void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerScalingModeDidChangeNotification
     
                                                  object:nil];
    [_moviePlayer stop];
    _moviePlayer = nil;
}
@end
