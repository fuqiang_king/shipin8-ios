//
//  SystemMsgCell.m
//  shipin8-ios
//
//  Created by gxw on 14-2-7.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "SystemMsgCell.h"

@implementation SystemMsgCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.layer.cornerRadius = 10.0f;
        self.contentView.frame = CGRectMake(40, 10, 200, 200);
        self.contentView.layer.borderWidth = 1.0f;
        self.contentView.layer.borderColor = [UIColor whiteColor].CGColor;
        self.contentView.backgroundColor = [UIColor grayColor];
        
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 190, 10)];
        self.title.font = [UIFont boldSystemFontOfSize:15.0f];  //UILabel的字体大小
        self.title.numberOfLines = 0;  //必须定义这个属性，否则UILabel不会换行
        self.title.textColor = [UIColor whiteColor];
        self.title.textAlignment = NSTextAlignmentLeft;  //文本对齐方式
        self.title.text = @"kahsdkjfhasdkjfhasdkfhalsdf";
        [self.contentView addSubview:self.title];
        
        self.content = [[UILabel alloc] initWithFrame:CGRectMake(5, 25, 190, 80)];
        self.content.font = [UIFont boldSystemFontOfSize:10.0f];  //UILabel的字体大小
        self.content.numberOfLines = 0;  //必须定义这个属性，否则UILabel不会换行
        self.content.textColor = [UIColor whiteColor];
        self.content.textAlignment = NSTextAlignmentLeft;  //文本对齐方式
        self.content.text = @"kahsdkjaskdhfakshdfaklsdhfkasdhffhasdkjfhasdkfhalsdf";
        [self.contentView addSubview:self.content];
    
        self.backgroundColor = [UIColor clearColor];
        self.frame = self.contentView.frame;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame {
    frame.origin.x += 40;
    frame.size.width -= 2 * 40;
    [super setFrame:frame];
}

@end
