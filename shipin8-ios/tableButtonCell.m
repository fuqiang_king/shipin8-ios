//
//  tableButtonCell.m
//  shipin8-ios
//
//  Created by gxw on 14-2-11.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "tableButtonCell.h"

@implementation tableButtonCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.layer.cornerRadius = 6.0f;
        self.contentView.frame = CGRectMake(10, 0, 300, 40);
        self.contentView.layer.borderWidth = 0.2f;
        self.contentView.layer.borderColor = [UIColor blackColor].CGColor;
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 40)];
        self.title.font = [UIFont boldSystemFontOfSize:15.0f];  //UILabel的字体大小
        self.title.textColor = [UIColor redColor];
        self.title.textAlignment = NSTextAlignmentCenter;  //文本对齐方式
        [self.contentView addSubview:self.title];
    }
    
    self.backgroundColor = [UIColor clearColor];
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame {
    frame.origin.x += 10;
    frame.size.width -= 2 * 10;
    [super setFrame:frame];
}

@end
