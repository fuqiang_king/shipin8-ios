//
//  MainViewController.m
//  shipin8-ios
//
//  Created by gxw on 14-1-22.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "MainViewController.h"
#import "VideosViewController.h"
#import "MessagesViewController.h"
#import "SettingsViewController.h"
#import "OtherViewController.h"

@interface MainViewController ()
@property (nonatomic,retain) VideosViewController *videosViewController;
@property (nonatomic,retain) MessagesViewController *messageViewController;
@property (nonatomic,retain) SettingsViewController *settingsViewController;
@property (nonatomic,retain) OtherViewController *otherController;
@end

@implementation MainViewController

- (void)loadView
{
    [super loadView];
    
    NSMutableArray *controllers = [NSMutableArray array];
    
    _videosViewController = [[VideosViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController: _videosViewController];
    nav.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"视频广场" image:[UIImage imageNamed:@"camcoder_pro_filled-32"] tag:0];
    [controllers addObject:nav];
    
    _messageViewController = [[MessagesViewController alloc] init];
    UINavigationController *nav_message = [[UINavigationController alloc] initWithRootViewController: _messageViewController];
    nav_message.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"消息" image:[UIImage imageNamed:@"topic-32"] tag:0];
    [controllers addObject:nav_message];
    
    _settingsViewController = [[SettingsViewController alloc] init];
    UINavigationController *nav_setting = [[UINavigationController alloc] initWithRootViewController: _settingsViewController];
    nav_setting.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"设备管理" image:[UIImage imageNamed:@"settings-32"] tag:0];
    [controllers addObject:nav_setting];
    
    _otherController = [[OtherViewController alloc] init];
    UINavigationController *nav_other = [[UINavigationController alloc] initWithRootViewController:_otherController];
    nav_other.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"更多" image:[UIImage imageNamed:@"online-32"] tag:0];
    [controllers addObject:nav_other];
    
    self.viewControllers = controllers;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
