//
//  BaseViewController.m
//  shipin8-ios
//
//  Created by gxw on 14-1-22.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "BaseViewController.h"
#import "User.h"
#import "version.c"
#import "GuidesViewController.h"
#import "MainViewController.h"

@interface BaseViewController ()
@property (nonatomic,retain) GuidesViewController *guideViewController;
@property (nonatomic,retain) MainViewController *mainViewController;
@end

@implementation BaseViewController

- (void)loadView
{
    [super loadView];
    
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.view.backgroundColor = [UIColor whiteColor];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (![[User current_app_version] isEqualToString:APP_VERSION]) {
        [User reset_app_version:APP_VERSION];
        
        _guideViewController = [[GuidesViewController alloc] init];
        _guideViewController.delegate = self;
        [self.view addSubview:_guideViewController.view];
    } else {
        [self showMainView];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showMainView
{
    _mainViewController = [[MainViewController alloc] init];
    [self.view addSubview:_mainViewController.view];
}

- (BOOL)shouldAutorotate{
    return NO;
}
@end
