//
//  GuidesViewController.h
//  shipin8-ios
//
//  Created by gxw on 14-1-22.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface GuidesViewController : UIViewController<UIScrollViewDelegate>
@property (nonatomic,retain) id delegate;
@end