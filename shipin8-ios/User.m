//
//  User.m
//  shipin8
//
//  Created by gxw on 14-1-22.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "User.h"

@implementation User
+(NSString*)current_app_version
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"APP_VERSION"];
}

+(void)reset_app_version:(NSString *)version
{
    [[NSUserDefaults standardUserDefaults] setValue:version forKey:@"APP_VERSION"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end