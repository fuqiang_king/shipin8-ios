//
//  EventMsgCell.h
//  shipin8-ios
//
//  Created by gxw on 14-2-7.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventMsgCell : UICollectionViewCell
@property (nonatomic,retain) UIImageView *imageView;
@property (nonatomic,retain) UITextView *msgType;
@property (nonatomic,retain) UITextView *msgMachine;
@property (nonatomic,retain) UITextView *msgTime;
@end
