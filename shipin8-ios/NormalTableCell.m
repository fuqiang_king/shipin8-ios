//
//  NormalTableCell.m
//  shipin8-ios
//
//  Created by gxw on 14-2-8.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "NormalTableCell.h"

@implementation NormalTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.layer.cornerRadius = 10.0f;
        self.contentView.frame = CGRectMake(20, 0, 280, 40);
        self.contentView.layer.borderWidth = 1.0f;
        self.contentView.layer.borderColor = [UIColor clearColor].CGColor;
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)setFrame:(CGRect)frame {
//    frame.origin.x += 20;
//    frame.size.width -= 2 * 20;
//    [super setFrame:frame];
//}


@end
