//
//  MessagesViewController.m
//  shipin8-ios
//
//  Created by gxw on 14-1-23.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "MessagesViewController.h"
#import "SystemMessagesViewController.h"
#import "EventMessagesViewController.h"

@interface MessagesViewController ()
@property (nonatomic,retain) UISegmentedControl *mySegment;
@property (nonatomic,retain) EventMessagesViewController *eventController;
@property (nonatomic,retain) SystemMessagesViewController *systemController;

- (void)segmentedAction:(id)sender;
@end

@implementation MessagesViewController

- (void)loadView
{
    [super loadView];
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    _mySegment = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"事件消息", @"系统消息", nil]];
    _mySegment.frame = CGRectMake(50, 25, 220, 30);
    _mySegment.selectedSegmentIndex = 0;
    
    [self.view addSubview:_mySegment];
    [self.navigationController.navigationBar.topItem setTitleView:_mySegment];
    
    [_mySegment addTarget:self
                   action:@selector(segmentedAction:)
                forControlEvents:UIControlEventValueChanged];
    
    _eventController = [[EventMessagesViewController alloc] init];
    _systemController = [[SystemMessagesViewController alloc] init];
    
    [self.view addSubview:_systemController.view];
    [self.view addSubview:_eventController.view];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)segmentedAction:(id)sender
{
    NSLog(@"the selectIndex is: %i",_mySegment.selectedSegmentIndex);
    if (_mySegment.selectedSegmentIndex == 0) {
        [[self.view.subviews objectAtIndex:1] setHidden:NO];
    } else {
        [[self.view.subviews objectAtIndex:1] setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
