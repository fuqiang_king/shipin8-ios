//
//  EventMsgCell.m
//  shipin8-ios
//
//  Created by gxw on 14-2-7.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "EventMsgCell.h"

@implementation EventMsgCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.contentView.layer.cornerRadius = 10.0f;
        self.contentView.frame = CGRectMake(0, 0, 285, 125);
        self.contentView.layer.borderWidth = 1.0f;
        self.contentView.layer.borderColor = [UIColor whiteColor].CGColor;
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        self.imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"contacts-128"]];
        self.imageView.layer.masksToBounds = YES;
        self.imageView.layer.cornerRadius = 10.0f;
        self.imageView.frame = CGRectMake(10, 15, 100, 75);
        [self.contentView addSubview:self.imageView];
        
        self.msgType = [[UITextView alloc] initWithFrame:CGRectMake(125, 15, 130, 25)];
        self.msgType.backgroundColor = [UIColor clearColor];
        self.msgType.editable = NO;
        [self.contentView addSubview:self.msgType];
        
        self.msgMachine = [[UITextView alloc] initWithFrame:CGRectMake(125, 42, 130, 20)];
        self.msgMachine.backgroundColor = [UIColor clearColor];
        self.msgMachine.editable = NO;
        [self.contentView addSubview:self.msgMachine];
        
        self.msgTime = [[UITextView alloc] initWithFrame:CGRectMake(125, 65, 130, 20)];
        self.msgTime.backgroundColor = [UIColor clearColor];
        self.msgTime.editable = NO;
        [self.contentView addSubview:self.msgTime];
        
        UIButton *button = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        //给定button在view上的位置
        
        button.frame = CGRectMake(0, 100, 142, 25);
        [button setTitle:@"本地视频" forState: UIControlStateNormal];
        
        
        //button背景色
        CALayer *layer = button.layer;
        layer.backgroundColor = [[UIColor clearColor] CGColor];
        layer.borderColor = [[UIColor darkGrayColor] CGColor];
        layer.borderWidth = 1.0f;
        
        [self.contentView addSubview:button];
        
        UIButton *button1 = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        //给定button在view上的位置
        
        button1.frame = CGRectMake(142, 100, 142, 25);
        [button1 setTitle:@"历史视频" forState: UIControlStateNormal];
        
        
        //button背景色
        CALayer *layer1 = button1.layer;
        layer1.backgroundColor = [[UIColor clearColor] CGColor];
        layer1.borderColor = [[UIColor darkGrayColor] CGColor];
        layer1.borderWidth = 1.0f;
        
        [self.contentView addSubview:button1];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
