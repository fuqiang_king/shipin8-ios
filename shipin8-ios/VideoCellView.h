//
//  VideoCellView.h
//  shipin8-ios
//
//  Created by gxw on 14-1-23.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoCellView : UICollectionViewCell
@property (nonatomic,retain) UIImageView *imageView;
@property (nonatomic,retain) UITextView *textView;
@end
