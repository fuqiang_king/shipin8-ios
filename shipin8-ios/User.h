//
//  User.h
//  shipin8
//
//  Created by gxw on 14-1-22.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
+(NSString*) current_app_version;
+(void) reset_app_version:(NSString*)version;
@end