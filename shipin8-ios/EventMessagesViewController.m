//
//  EventMessagesViewController.m
//  shipin8-ios
//
//  Created by gxw on 14-1-24.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "EventMessagesViewController.h"
#import "EventMsgCell.h"
#import "MJRefresh.h"

@interface EventMessagesViewController()<MJRefreshBaseViewDelegate>
{
    MJRefreshHeaderView *_header;
    MJRefreshFooterView *_footer;
}
@property UICollectionView *collectionView;

@end

@implementation EventMessagesViewController

- (void)loadView
{
    [super loadView];
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.view.backgroundColor = [UIColor blueColor];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    //每个cell的大小
    [flowLayout setItemSize:CGSizeMake(285,125)];
    
    //设置方向（向下还是向右）垂直Vertical  水平Horizontal
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    //设置距离边框的高度
    flowLayout.sectionInset = UIEdgeInsetsMake(10, 0, 10, 0);//top、lef、bottom、right
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame collectionViewLayout:flowLayout];
    
    NSLog(@"the origin is %f",[UIScreen mainScreen].applicationFrame.origin.y);
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.alwaysBounceVertical = YES;
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.collectionView];
    
    [self.collectionView registerClass:[EventMsgCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.collectionView reloadData];
    
    if ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0f) {
        self.collectionView.frame = CGRectMake(0, self.collectionView.frame.origin.y + 45, self.collectionView.frame.size.width, self.collectionView.frame.size.height - 90);
    } else {
        self.collectionView.frame = CGRectMake(0, self.collectionView.frame.origin.y, self.collectionView.frame.size.width, self.collectionView.frame.size.height - 90);
    }
    
    // 3.集成刷新控件
    // 3.1.下拉刷新
    MJRefreshHeaderView *header = [MJRefreshHeaderView header];
    header.scrollView = self.collectionView;
    header.delegate = self;
    // 自动刷新
    [header beginRefreshing];
    _header = header;
    
    // 3.2.上拉加载更多
    MJRefreshFooterView *footer = [MJRefreshFooterView footer];
    footer.scrollView = self.collectionView;
    footer.delegate = self;
    _footer = footer;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return 5;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    EventMsgCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (indexPath.row > 1) {
        cell.imageView.image = [UIImage imageNamed:@"forward2-128"];
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height + 100);
    } else {
        cell.imageView.image = [UIImage imageNamed:@"contacts-128"];
    }
    cell.msgTime.text = [@(indexPath.row) stringValue];
    cell.msgMachine.text = [@(indexPath.row) stringValue];
    cell.msgType.text = @"发生了侵入事件";
    
    
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    EventMsgCell *cell =[collectionView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor redColor];
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    EventMsgCell *cell =[collectionView cellForItemAtIndexPath:indexPath];
    cell.contentView.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.contentView.backgroundColor = [UIColor underPageBackgroundColor];
}

- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [self.collectionView reloadData];
    
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

#pragma mark - 刷新控件的代理方法
#pragma mark 开始进入刷新状态
- (void)refreshViewBeginRefreshing:(MJRefreshBaseView *)refreshView
{
    NSLog(@"%@----开始进入刷新状态", refreshView.class);
    // 2.2秒后刷新表格UI
    [self performSelector:@selector(doneWithView:) withObject:refreshView afterDelay:2.0];
}

#pragma mark 刷新完毕
- (void)refreshViewEndRefreshing:(MJRefreshBaseView *)refreshView
{
    NSLog(@"%@----刷新完毕", refreshView.class);
}

#pragma mark 监听刷新状态的改变
- (void)refreshView:(MJRefreshBaseView *)refreshView stateChange:(MJRefreshState)state
{
    switch (state) {
        case MJRefreshStateNormal:
            NSLog(@"%@----切换到：普通状态", refreshView.class);
            break;
            
        case MJRefreshStatePulling:
            NSLog(@"%@----切换到：松开即可刷新的状态", refreshView.class);
            break;
            
        case MJRefreshStateRefreshing:
            NSLog(@"%@----切换到：正在刷新状态", refreshView.class);
            break;
        default:
            break;
    }
}

/**
 为了保证内部不泄露，在dealloc中释放占用的内存
 */
- (void)dealloc
{
    [_header free];
    [_footer free];
}

@end
