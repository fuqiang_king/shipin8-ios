//
//  VideoCellView.m
//  shipin8-ios
//
//  Created by gxw on 14-1-23.
//  Copyright (c) 2014年 gxw. All rights reserved.
//

#import "VideoCellView.h"

@implementation VideoCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.contentView.layer.cornerRadius = 10.0f;
        self.contentView.frame = CGRectMake(0, 0, 280, 200);
        self.contentView.layer.borderWidth = 1.0f;
        self.contentView.layer.borderColor = [UIColor whiteColor].CGColor;
        self.contentView.backgroundColor = [UIColor underPageBackgroundColor];
        
        self.imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"contacts-128"]];
        self.imageView.layer.masksToBounds = YES;
        self.imageView.layer.cornerRadius = 10.0f;
        self.imageView.frame = CGRectMake(50, 35, 128, 128);
        [self.contentView addSubview:self.imageView];
        
        self.textView = [[UITextView alloc] initWithFrame:CGRectMake(20, 1, 100, 23)];
        self.textView.backgroundColor = [UIColor clearColor];
        self.textView.editable = NO;
        self.textView.text = @"客厅摄像头";
        [self addSubview:self.textView];
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
